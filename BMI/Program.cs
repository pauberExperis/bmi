﻿using System;

namespace BMI
{
    class Program
    {
        static void Main(string[] args)
        {
            StartMethod();
        }

        public static void StartMethod()
        {
            Console.WriteLine("Enter Weight in kg:");
            string weight = Console.ReadLine();
            float wgt = float.Parse(weight);
            Console.WriteLine("Enter Height in cm:");
            string height = Console.ReadLine();
            float hgt = float.Parse(height) / 100;
            float bmi = CalculateBMI(wgt, hgt);
            DisplayBMI(bmi);
        }
        public static float CalculateBMI(float w, float h)
        {
            return w / (h * h);
        }

        public static void DisplayBMI(float b)
        {
            int switcheroo;
            if (b < 18.5)
            {
                switcheroo = 0;
            }
            else if (b < 24.9)
            {
                switcheroo = 1;
            }
            else if (b < 29.9)
            {
                switcheroo = 2;
            }
            else
            {
                switcheroo = 3;
            }
            switch (switcheroo)
            {
                case 0:
                    Console.WriteLine("You are UNDERWEIGHT! BMI: " + b);
                    break;
                case 1:
                    Console.WriteLine("You are NORMAL WEIGHT! BMI: " + b);
                    break;
                case 2:
                    Console.WriteLine("You are OVERWEIGHT! BMI: " + b);
                    break;
                case 3:
                    Console.WriteLine("You are OBESE! BMI: " + b);
                    break;
            }
            EndMethod();
        }

        public static void EndMethod()
        {
            Console.WriteLine("Enter Y to try again, or X to exit!");
            string result = Console.ReadLine();
            if (result.ToLower() == "y")
            {
                StartMethod();
            } else if (result.ToLower() == "x")
            {
                Console.WriteLine("Goodbye!");
            } else
            {
                Console.WriteLine("Unrecognised input, try again");
                EndMethod();
            }
        }
    }
}
